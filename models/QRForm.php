<?php


namespace app\models;

use Yii;
use yii\base\Model;

class QRForm extends Model
{
    public $Red;
    public $Green;
    public $Blue;
    public $color;
    public $prefix;
    public $numberOfFiles;
    public $numberOfCodes;

    public function rules()
    {
        return [
            [['prefix', 'numberOfFiles', 'numberOfCodes', 'color'], 'required'],
            ['numberOfCodes', 'integer'],
            ['prefix', 'string'],
            ['numberOfFiles', 'integer']
        ];
    }
}