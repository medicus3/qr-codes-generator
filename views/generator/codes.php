<?php
/* @var $this yii\web\View */
/* @var $model \app\models\QRForm */

/* @var $qrCode \Da\QrCode\QrCode */

use Da\QrCode\QrCode;
use yii\helpers\Html;

$this->title = 'QRGenerator';
$this->params['breadcrumbs'][] = $this->title;

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div style="display: flex;flex-direction: row;flex-flow: row wrap;justify-content: center;align-items: flex-start;text-align: center">

    <?php
    for ($i = 0; $i < $model->numberOfCodes; $i++) {
        $qrCode = (new QrCode($model->prefix . '' . microtime(true)))
            ->setSize(100)
            ->setMargin(5)
            ->useForegroundColor($model->Red, $model->Green, $model->Blue)->writeDataUri();
        echo
            '<div style="flex-grow: 3;margin: auto;">
                          <img src="' . $qrCode . '">        
                      </div>';
    }
    ?>

</div>


</body>
</html>
