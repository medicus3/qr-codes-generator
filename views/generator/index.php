<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\QRForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\color\ColorInput;

$this->title = 'QRGenerator';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <h1><?= Html::encode($this->title) ?></h1>

    <!-- Build Form for generate QRCode file -->
    <?php $form = ActiveForm::begin([
        'id' => 'qr-form',
        'layout' => 'horizontal',
    ]); ?>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
                'useNative' => true,
                'options' => ['placeholder' => 'Select color ...'],
            ]); ?></div>

        <?= $form->field($model, 'prefix') ?>

        <?= $form->field($model, 'numberOfFiles')->input('number') ?>

        <?= $form->field($model, 'numberOfCodes')->input('number') ?>

    </div>


    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Generate', ['class' => 'btn btn-primary', 'name' => 'qr-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>
