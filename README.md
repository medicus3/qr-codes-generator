<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Project</h1>
    <br>
</p>

QRCode Generator Application that let you set up your configuration for each,
file that contain QR codes.

REQUIREMENTS
------------

Minimum PHP 5.6.0.


RUNNING
------------

- Composer install
- php yii serve --port=8000

