<?php


namespace app\controllers;


use app\models\QRForm;
use Yii;
use Da\QrCode\QrCode;
use mPDF;
use yii\base\BaseObject;
use yii\web\Controller;
use ZipArchive;

class GeneratorController extends Controller
{

    public function actionIndex()
    {
        $model = new QRForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $hex = "#ff9900";
            list($r, $g, $b) = sscanf($hex, "#%02x%02x%02x");
            $model->Red = $r;
            $model->Green = $g;
            $model->Blue = $b;
            $zip = new ZipArchive();
            if($zip->open(Yii::getAlias('@runtime/temp.zip'),ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) !== TRUE){
                die ("An error occurred creating your ZIP file.");
            }
            if ($zip->open(Yii::getAlias('@runtime/temp.zip'),ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {

                for ($i = 0; $i < $model->numberOfFiles; $i++) {
                    $firstName = Yii::getAlias('@runtime/'.microtime(true));
                    $mpdf = new \Mpdf\Mpdf(['c', 'A4', '', '', 32, 25, 27, 25, 16, 13]);
                    $mpdf->Bookmark('Start of the document');
                    $mpdf->SetDisplayMode('fullpage');
                    $mpdf->setHeader('QRCodes           ');
                    $mpdf->WriteHTML($this->renderPartial('codes', ['model' => $model]));
                    $mpdf->Output("$firstName$i.pdf", 'F');
                    $zip->addFile("$firstName$i.pdf", "$i.pdf");

                }
            } else {
                echo "cant open!!";
                exit;
            }
            $zip->close();
            $archfilename = Yii::getAlias('@runtime/temp.zip');
            header("Content-type: application/zip");
            header("Content-Disposition: attachment; filename = collection.zip");
            header("Pragma: no-cache");
            header("Expires: 0");
            readfile("$archfilename");
            exit();
        }
        return $this->render('index', [
            'model' => $model
        ]);
    }

}